FROM python:3.7

WORKDIR /app

COPY . /app

RUN pip3 install flask
RUN pip3 install pylint

CMD python3 hello.py
